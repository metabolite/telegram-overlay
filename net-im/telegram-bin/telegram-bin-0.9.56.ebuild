# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils versionator

DESCRIPTION="Official telegram protocol client"
HOMEPAGE="https://telegram.org/"
SRC_URI="amd64? ( https://updates.tdesktop.com/tlinux/tsetup.${PV}.tar.xz -> ${P}.tar.xz )
	 x86? ( https://updates.tdesktop.com/tlinux32/tsetup32.${PV}.tar.xz -> ${PN}32-${PV}.tar.xz )"

RESTRICT="mirror strip"
LICENSE="GPL-3"
SLOT="0"
IUSE="updater"
KEYWORDS="~amd64 ~x86"
INSTALL_DIR="/opt/telegram"

DEPEND="dev-libs/libappindicator
	x11-libs/gtk+:2"
RDEPEND="${DEPEND}"

QA_PREBUILT="opt/bin/${PN}"
S="${WORKDIR}/Telegram"

src_install() {
    insinto "${INSTALL_DIR}"
    insopts -m755
    doins -r Telegram
    make_wrapper "telegram" "${INSTALL_DIR}/Telegram"
    make_desktop_entry "telegram" "Telegram" "telegram" "Messenger"
}

